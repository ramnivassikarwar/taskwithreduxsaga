import NetInfo from '@react-native-community/netinfo';
import flashMessage from '../components/common/CustomFlashAlert';

/**
 * this funciton return api response ( promise )
 *
 * @param {string} apiUrl for fetching data
 * @param {object} reqObj is an json object that contains request for call Api
 */
export const fetchData = async (apiUrl, reqObj) => {
  let networkStatus;
  await NetInfo.fetch().then((state) => {
    networkStatus = state.isConnected;
  });
  if (networkStatus) {
    return await fetch(apiUrl, reqObj)
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        }
        return new Promise((res, rej) => rej(response.statusText));
      })
      .catch(async (error) => {
        return new Promise((res, rej) => rej(error));
      });
  } else {
    return flashMessage('You are offline.', 'danger');
  }
};
