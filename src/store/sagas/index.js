import {all, takeLatest} from 'redux-saga/effects';
import {taskListSaga} from './saga';
import {TASK_LIST_DATA} from '../actions/actionTypes';
function* watchLTaskListAction() {
  yield takeLatest(TASK_LIST_DATA, taskListSaga);
}
function* rootSaga() {
  yield all([watchLTaskListAction()]);
}
export default rootSaga;
