import {put} from 'redux-saga/effects';
import {TASK_LIST_DATA_SUCCESS} from '../actions/actionTypes';

import { TASK_LIST_DATA_FAILURE} from '../../constants/apis';

export function* taskListSaga({payload}) {
  try {
   
   
    yield put({type: TASK_LIST_DATA_SUCCESS, payload});
  } catch (e) {
    yield put({type: TASK_LIST_DATA_FAILURE});
  }
}
