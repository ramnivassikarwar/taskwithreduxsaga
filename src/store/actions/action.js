import {TASK_LIST_DATA} from '../actions/actionTypes';

export function taskListAction(data) {
  return {
    type: TASK_LIST_DATA,
    payload: data,
  };
}
