// Task List Action types for sagas
export const TASK_LIST_DATA = 'TASK_LIST_DATA';
// Task Action types for reducers
export const TASK_LIST_DATA_SUCCESS = 'TASK_LIST_DATA_SUCCESS';
export const  TASK_LIST_DATA_FAILURE = 'TASK_LIST_DATA_FAILURE';
