import {combineReducers} from 'redux';
import {
  TASK_LIST_DATA_SUCCESS,
  TASK_LIST_DATA_FAILURE,
} from '../actions/actionTypes';

function taskListReducer(state = [], action) {
  switch (action.type) {
    case TASK_LIST_DATA_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}
function error(state = '', action) {
  switch (action.type) {
    case TASK_LIST_DATA_FAILURE:
      return {error: action.error};
    default:
      return state;
  }
}

export default combineReducers({
  taskListReducer,
  error,
});
