import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import config from '../config';
import { TaskList ,AddEditTask} from '../screens';

const RootStack = createNativeStackNavigator();

const RootNavigation = () => {
 
  return (
    <RootStack.Navigator
      >
      <RootStack.Screen
        name={config.routes.LANDING}
        component={TaskList}
        options={{ headerShown: false }}
      />
       <RootStack.Screen
        name={config.routes.ADDEDITTASK}
        component={AddEditTask}
      
      />  
    </RootStack.Navigator>
  );
};

export default RootNavigation;
