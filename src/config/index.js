import colors from './colors';

import routes from './routes';
import strings from './strings';

const config = {
  colors,
  routes,
  strings,
};

export default config;
