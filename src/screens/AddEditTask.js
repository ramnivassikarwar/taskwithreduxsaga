/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useEffect, useState,useCallback} from 'react';
import {useSelector, useDispatch, shallowEqual} from 'react-redux';
import {View, Text,  TextInput,StyleSheet,TouchableOpacity,Alert} from 'react-native';
import config from '../config';
import {taskListAction} from '../store/actions/action';

 const AddEditTask = ({ navigation, route }) => {
  const { item:selectedItem } = route.params;
console.log("taskItem: ",selectedItem);
  
const dispatch = useDispatch();
  const [taskName, setTaskName] = useState(selectedItem?selectedItem.taskName:'');
  const {taskListReducer} = useSelector(({taskListReducer:{taskListReducer,error}}) => ({
    taskListReducer,
    error,
}), shallowEqual);

const onSubmitPress = ()=>
{
  if(taskName.length>0)
  {
  const taskList=[...taskListReducer];
  if(selectedItem)
  {
taskList.map((item)=>{
  if(item.id===selectedItem.id)
  {
    item.taskName=taskName;
  }
  return item;
})
  }
  else
  {
    taskList.push({taskName,id:Math.random()});
  }
  dispatch(taskListAction(taskList));
  navigation.goBack();
}
else
{
  Alert.alert(
    "Error",
    "Please enter task name",
    [
     
      { text: "OK",  }
    ]
  );
}
};

const onTextChange =
  e => {
    setTaskName(e.nativeEvent.text);
  };
    return (
      <>
        <TextInput style={main.textName} value={taskName} placeholder="Enter Task Name" onChange={onTextChange}></TextInput>
        <TouchableOpacity
       style={main.addContainer}
          onPress={onSubmitPress}>
          <Text style={main.addText}>{'Submit'}</Text>
        </TouchableOpacity>
      </>);

};



const main = StyleSheet.create({
  textName:{justifyContent:'center',padding:10},
  addContainer:{justifyContent:'center',alignItems:'center'},
  addText:{padding:10,color:config.colors.RUSSIAN_BLACK},
 
});

export default AddEditTask;
