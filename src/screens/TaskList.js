/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch, shallowEqual} from 'react-redux';
import {View, Text, TouchableOpacity,StyleSheet,Dimensions} from 'react-native';
import {taskListAction} from '../store/actions/action';
import flashMessage from '../components/common/CustomFlashAlert';
import {SwipeListView, SwipeRow} from 'react-native-swipe-list-view';
import config from '../config';
import LinearGradient from 'react-native-linear-gradient';

const SWIPE_RIGHT_MARGIN =130;
const FACTOR = 0.07;
const height = Dimensions.get('screen').height;
const CONTACT_HEIGHT = height * FACTOR;
const PADDING_HORIZONTAL = 20;

 const TaskList = ({  navigation, route }) => {
  const dispatch = useDispatch();

  const {taskListReducer:taskLitItems} = useSelector(({taskListReducer:{taskListReducer,error}}) => ({
    taskListReducer,
    error,
}), shallowEqual);

console.log("taskLitItems: ",taskLitItems);

const onEditPress = (rowMap, item)=>
{
  navigation.navigate(config.routes.ADDEDITTASK, {
    item
  });
};

const onAddPress = ()=>
{
  navigation.navigate(config.routes.ADDEDITTASK,{});
};

const onDeletePress = ( selectedItem) => {
  const taskList= [...taskLitItems].filter((item)=>{
    return item.id !==selectedItem.id;
 })
 dispatch(taskListAction(taskList));
  };

  const renderHiddenItem = (data) => {


    return (
      <LinearGradient
        colors={[config.colors.CERISE_RED, config.colors.CERISE_RED_1]}
        locations={[0.2, 0.5]}
        style={styles.main.rowBack}
        angle={-90}
        useAngle>
          <View style={styles.main.backRightBtnRight}>
        <TouchableOpacity
         style={{marginRight:10,}}
          onPress={() => onEditPress( data.item)}>
          <Text style={styles.main.backTextWhite}>{'Edit'}</Text>
        </TouchableOpacity>
        <TouchableOpacity
         
          onPress={() => onDeletePress( data.item)}>
          <Text style={styles.main.backTextWhite}>{'Delete'}</Text>
        </TouchableOpacity>
        </View>
      </LinearGradient>
    );
  };



 const getBlockItem = (rowData) => {
    const ratio = 1;
    return (
      <View style={styles.main.blockItemContainer}>
        <SwipeRow
          style={styles.main.swipeRowStyle}
          rightOpenValue={ratio >= 1 ? -SWIPE_RIGHT_MARGIN : -1000}
          initialRightActionState={true}
          initialLeftActionState={true}>
          {renderHiddenItem(rowData)}
          <RenderTaskListData
            item={rowData.item}
            
          />
        </SwipeRow>
      </View>
    );
  };



    return (
      <>
       

<View style={styles.main.content}>
    <TouchableOpacity
       style={styles.main.addContainer}
          onPress={onAddPress}>
          <Text style={styles.main.addText}>{'Add'}</Text>
        </TouchableOpacity>

            <SwipeListView
              data={taskLitItems}
              renderItem={getBlockItem}
              rightOpenValue={-SWIPE_RIGHT_MARGIN}
              keyExtractor={item => item.id + ''}
              previewRowKey={'0'}
             
              previewOpenValue={-60}
              previewOpenDelay={3000}
            
            />
          </View>
      </>
      
      
      
      );

};

 export const RenderTaskListData = (itemT, index)=> {
  const item=itemT.item;

  return (
          <View style={{  justifyContent: 'center',backgroundColor:config.colors.WHITE }}>
              <Text
                  style={{
                   padding:20,
                    fontSize: 18,
                    textAlign: 'left',
                   flex:1,
                    color: 'black',
                  }}
              >
                { item.taskName}
              </Text>
           

          </View>
     );
};



const main = StyleSheet.create({
  addContainer:{justifyContent:'center',alignItems:'center'},
  addText:{padding:10,color:config.colors.RUSSIAN_BLACK},
  searchContainer: {flex: 1},
  contactContainer: {marginBottom: 14, marginHorizontal: PADDING_HORIZONTAL},
  emptyAlpha: {height: 0.1},
  swipeRowStyle: {marginBottom: 14, width: '100%'},
  blockItemContainer: {flexDirection: 'row', alignItems: 'center'},
  container: {
    flex: 1,
    backgroundColor:config.colors. RUSSIAN_BLACK,
  },
  content: {
    position: 'relative',
    flex: 1,
    width: '100%',
    marginTop: 20,
  },


  backRightBtnRight: {
    right: 0,
    color:config.colors. WHITE,
    zIndex: 99,
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    justifyContent: 'center',
    top: 0,
    width: SWIPE_RIGHT_MARGIN,
    flexDirection:'row',
  },
  rowBack: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',

    height: CONTACT_HEIGHT,
    marginHorizontal: 14,
    borderRadius: 20,
  },
  backTextWhite: {
    color:config. colors.WHITE,
  },
 
});

const styles = {
  main,
};
export default TaskList;
