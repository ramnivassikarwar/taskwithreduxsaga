import  React , {useState} from 'react';
import {SafeAreaView,StyleSheet} from 'react-native';
import {RootNavigation} from './src/navigations';
import {Provider} from 'react-redux';
import configureStore from './src/store/configStore';
import FlashMessage from 'react-native-flash-message';
import { NavigationContainer } from '@react-navigation/native';
const store = configureStore();

const App = () =>   {

    return (
        <Provider store={store}>
          <NavigationContainer>
          <SafeAreaView style ={ styles.container}>
            <RootNavigation />
             <FlashMessage position="top" />
          </SafeAreaView>
            </NavigationContainer>
           
        </Provider>
     
    );
  
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
